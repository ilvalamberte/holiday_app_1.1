 /* eslint-disable */ 
import React, {useState, useContext} from 'react'
import HolidayContext from '../../context/holidayContext/HolidayContext.js'
import CompNav from '../layout/CompNav.js'
import { FcCalendar } from 'react-icons/fc'
import DatePicker from "react-datepicker";




const Request = () => {

  const {addHoliday, holidays} = useContext(HolidayContext);

  const [startDate, setStartDate] = useState(new Date("2014/02/08"));
  const [endDate, setEndDate] = useState(new Date("2014/02/10"));
  const [holiday, setHoliday] = useState({
    start: '',
    end: '',
    type: '',
    title: '',
    comment: ''
  })
  const {start, end, type, title, comment} = holiday
  const handleChange = (e) => {
    setHoliday({
      ...holiday,
      [e.target.name]: e.target.value
    })
  }
  const onsubmit = (e) => {
    e.preventDefault();
    console.log(holiday);
    addHoliday(holiday);
    console.log(holidays)
    setHoliday({
      start: '',
      end: '',
      type: '',
      title: '',
      comment: ''
    })

  }

    return (  
        <body>
        <div className="container">
        <CompNav />
        
          <div className="main">
        
            <div className="section">
            <div className="mail-detail">
 
 <div className="mail-contents">
  <div className="mail-contents-subject">
   <input type="checkbox" name="msg" id="mail20" className="mail-choice" checked />
   <label for="mail20"></label>

  </div>
  <div className="">
  
   < FcCalendar />
   

    
   </div>
   




   <div className="mail-checklist">
   <input type="date" name='start' value={start} placeholder="Date from" onChange={handleChange}/>

   </div>
   <div className="mail-checklist">
   <input type="date" name='end' value={end} placeholder="Date to" onChange={handleChange}/>

   </div>
   <div className="mail-checklist">
   <input type="text" name='type' value={type} placeholder="Type of Holiday" onChange={handleChange}/>

   </div>
   <div className="mail-checklist">
   <input type="text" name='title' value={title} placeholder="Holiday Title" onChange={handleChange}/>
   </div>

   <div className="mail-checklist">
   <input type="text" name='comment' value={comment} placeholder="Write a comment..." onChange={handleChange}/>

   </div>

   <div class="buttons">

<div class="button button-primary" onClick={onsubmit}>Send</div>

</div>

  </div>
 </div>

 </div>
        
            </div>
          </div>

        </body>

        
    )

}

export default Request