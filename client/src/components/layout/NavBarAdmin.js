import React, {useContext, useEffect} from 'react'
import AuthContext from '../../context/authContext/authContext.js'

import {FcCalendar} from 'react-icons/fc'
import {FcBusinessman} from 'react-icons/fc'
import {FcTimeline} from 'react-icons/fc'
import {FcServices} from 'react-icons/fc'
import {FcPlus} from 'react-icons/fc'

import {BrowserRouter as Router, NavLink} from 'react-router-dom'
import Request from '../employee/Request.js'
import Navbar from './Navbar.js'

const NavBarAdmin = () => {


    return (
        <div className="aside">
        <div className="user">
          <div className="user__info">
            <p>Admin Dashboard</p>
          </div>
        </div>
        <nav>
        <ul>
       
          <li>
            <FcBusinessman />
            <NavLink to='/admin'>
            <span>Requests</span>
            </NavLink>
          </li>
          <li>
        <FcTimeline />
        <NavLink to='/admincount'>
            <span>Total Stats</span>
            </NavLink>
          </li>
          
          <li>
        < FcPlus />
        <NavLink to='/request'>
            <span>Add Holiday</span>
        </NavLink>
          </li>
      
          <li>
        <FcServices />
        <NavLink to='/editpr'>
            <span>Edit Profile</span>
        </NavLink>
  </li>
  <li>
          
  </li>
        </ul>
      </nav>
      </div>
    )


} 

export default NavBarAdmin