 /* eslint-disable */ 
 import React, {useContext, useEffect, useState} from 'react';
 import AdminEntry from './AdminEntry.js'
 import NavBarAdmin from '../../components/layout/NavBarAdmin.js'
 import HolidayContext from '../../context/holidayContext/HolidayContext.js'
 
 
 const AdminPage = () => {

   const { holidays, getHolidays, removeHoliday, updateHoliday} = useContext(HolidayContext);
   console.log(holidays);

  /*  const id = holidays.map(holiday => holiday._id)
   const user = holidays.map(holiday => holiday.user)
   const comment = holidays.map(holiday => holiday.comment)
   const  status = holidays.map(holiday => holiday.status)
   const  title = holidays.map(holiday => holiday.title)
   const type = holidays.map(holiday => holiday.type)
   const start = holidays.map(holiday => holiday.start)
   const end = holidays.map(holiday => holiday.end) */

   useEffect (() => {
     getHolidays();
   }, []);



     return (
       <div className="adminContainer">
         <NavBarAdmin />
       <div className="sectionAdmin">
         {/* pass in props for each object, so can be destructured! */}
                   {holidays.map(holiday =>  
                   <AdminEntry  holiday={holiday}
                   key={holiday._id} />   
            
         )}
     
     </div>
     </div>
     )
 
 }
 
 export default AdminPage