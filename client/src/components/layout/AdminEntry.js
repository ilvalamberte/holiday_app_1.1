 /* eslint-disable */ 
import React, {useContext} from 'react'
import HolidayContext from '../../context/holidayContext/HolidayContext.js'
import moment from 'moment';
import { GrUserExpert } from 'react-icons/gr'
import { BsCheckBox } from 'react-icons/bs'
import { BsTrash } from 'react-icons/bs'
import { FcCellPhone } from 'react-icons/fc'



const AdminEntry = ({holiday}) => {
    const {removeHoliday, updateHoliday, editHoliday} = useContext(HolidayContext)
    const {_id, user, type, comment, start, end, status} = holiday

    /* const startDate = start.getDate() */

    const handleIsConfirmed = () => {
        updateHoliday({...holiday, status: true});

    }

    const handleIsRejected = () => {
        updateHoliday({...holiday, status:false})
    }

    const startDate = moment(start).format('MMMM Do YYYY');
    const endDate = moment(end).format('MMMM Do YYYY')
    console.log(startDate)

    return (
        <div className="cardAdmin">
            <div className="card-head">
            <button className="btn-card">
            <BsTrash />
            DELETE
            </button>
            <button className="btn-card" onClick={handleIsConfirmed}>
            <BsCheckBox />
            CONFIRM
            </button>
            <button className="btn-card" onClick={handleIsRejected}>
            <BsCheckBox />
            REJECT
            </button>
            </div>
            <div>
  <ul className="listAdmin">
  <li className="mail-checklist card__name" value={status}>{status=== true ? 'CONFIRMED' : 'REJECTED'}</li>
        <li className="card__name" value={type}>{type}</li>
        <li className="card__country" value={comment}>{comment}</li>
         <li className="card__country" value={start}>{startDate} - {endDate}</li>
        {/*  <li className="card__country" value={_id}>{_id}</li> */}
  </ul>

</div>
        </div>
    )
}

export default AdminEntry