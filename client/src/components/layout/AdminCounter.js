/* eslint-disable */
import React, {useContext, useEffect} from 'react';
import NavBarAdmin from './NavBarAdmin.js'

import HolidayContext from '../../context/holidayContext/HolidayContext.js'
import { Bar, Line, Pie } from 'react-chartjs-2'
import { CSVLink, CSVDownload } from "react-csv";

const AdminCounter = () => {
    const {getHolidays, holidays} = useContext(HolidayContext)

    useEffect ( () => {
        getHolidays()
    }, [])

    console.log(holidays);

    const approvedHolidays = holidays.filter(holiday => holiday.status === true);
    const rejectedHolidays = holidays.filter(holiday => holiday.status === false);
    const businessTrip = holidays.filter(holiday => holiday.type === 'Business Trip')
    const totalAmount = holidays.length
    console.log(totalAmount)

    const csvData = [
        ...approvedHolidays,
     	...rejectedHolidays,
		...businessTrip
      ];

    return (

    <div className="adminContainer">
         <NavBarAdmin />
       <div className="sectionData">
	
	<div class="flex-table">
		<div class="flex-table--header">
			<div class="flex-table--categories">
				<span class="flex-table_checkbox">
				</span>
				<span>Approved Holiday</span>
				<span>Rejected Holiday</span>
				<span>Business Trip</span>
				<span>Total Requests</span>
			</div>
		</div>
		<div class="flex-table--body">

			<div class="flex-table--row">
				<span class="flex-table_checkbox">
				
				</span>
				<span>{approvedHolidays.map(holiday =>	<span> {holiday.title} </span>)}</span>
				<span>{rejectedHolidays.map(holiday => <span> {holiday.title} </span>)}</span>
				<span>{businessTrip.map(holiday => <span> {holiday.title} </span>)}</span>
				<span>{totalAmount}</span>
			</div>

	</div>
	</div>
	
	   <CSVLink className="button button-primary" data={csvData}>Download</CSVLink>


</div>
</div>


    )
} 
export default AdminCounter