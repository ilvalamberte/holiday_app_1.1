/* eslint-disable */
import React, {useContext, useEffect} from 'react';

import HolidayContext from '../../context/holidayContext/HolidayContext.js'
import { Bar, Line, Pie } from 'react-chartjs-2'
import { CSVLink, CSVDownload } from "react-csv";

const AdminCounter = () => {
    const {getHolidays, holidays} = useContext(HolidayContext)

    useEffect ( () => {
        getHolidays()
    }, [])

    console.log(holidays);

    const approvedHolidays = holidays.filter(holiday => holiday.status === true);
    const rejectedHolidays = holidays.filter(holiday => holiday.status === false);
    const businessTrip = holidays.filter(holiday => holiday.type === 'Business Trip')
    const totalAmount = holidays.length
    console.log(totalAmount)

    return (
<div class="container">
	<h1>Responsive Flex Table</h1>
	
	<div class="flex-table">
		<div class="flex-table--header">
			<div class="flex-table--categories">
				<span class="flex-table_checkbox">
					<input id="c-all" type="checkbox"/>
					<label for="c-all"><span></span></label>
				</span>
				<span>Approved Holiday</span>
				<span>Rejected Holiday</span>
				<span>Business Trip</span>
				<span>Total Requests</span>
			</div>
		</div>
		<div class="flex-table--body">

			<div class="flex-table--row">
				<span class="flex-table_checkbox">
					<input id="c-1" type="checkbox"/>
					<label for="c-1"><span></span></label>
				</span>
				<span>{approvedHolidays.map(holiday => holiday.title)}</span>
				<span>{rejectedHolidays.map(holiday => holiday.title)}</span>
				<span>{businessTrip.map(holiday => holiday.title)}</span>
				<span>{totalAmount}</span>
			</div>


			<div class="flex-table--row">
				<span class="flex-table_checkbox">
					<input id="c-2" type="checkbox"/>
					<label for="c-2"><span></span></label>
				</span>
				<span>André Tronca</span>
				<span>24yo</span>
				<span>Caxias do Sul</span>
				<span>Rio Grande do Sul</span>
			</div>
	
	</div>
	
</div>
</div>

    )
} 
export default AdminCounter