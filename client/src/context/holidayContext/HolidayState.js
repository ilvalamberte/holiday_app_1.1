 /* eslint-disable */
import React, {useReducer} from 'react';
import axios from 'axios';
import HolidayContext from '../holidayContext/HolidayContext.js'
import HolidayReducer from '../holidayContext/HolidayReducers.js'
import {
    GET_HOLIDAYS,
    TOGGLE_FILTER,
    SEARCH_USER, 
    CLEAR_SEARCH,
    ADD_HOLIDAY,
    REMOVE_HOLIDAY,
    UPDATE_HOLIDAY
} from '../types.js'

const HolidayState = (props) => {
    const initialState = {
        filterGuest: false,
        search: null,
        holidays: [],
        users: [
            {
                name:  "Signe",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/women/65.jpg"

            },
            {
                name:  "Anne",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/women/34.jpg"
                
            },
            {
                name:  "Daniel",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/men/9.jpg"
                
            },
            {
                name:  "Soren",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/men/78.jpg"
                
            },
            {
                name:  "Simon",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/men/67.jpg"
                
            },
            {
                name:  "Signe",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/women/52.jpg"
                
            }
        ],
    }

    const [state, dispatch] = useReducer(HolidayReducer, initialState)

    const getHolidays = async () => {
        try {
            const res = await axios.get('http://localhost:5000/holidays');
           
            dispatch ({
                type : GET_HOLIDAYS,
                payload: res.data
            })

        } catch (err) {
            console.log(err)
        }
    }

    const addHoliday = async (holiday) => {
        holiday.id = Date.now();
        holiday.status = false;
        console.log(holiday);

        const config = {
            header: {
                'Content-Type': 'application/json'
            }
        }
        try {
            const res = await axios.post ('http://localhost:5000/holidays', holiday, config)
            console.log(res);
        dispatch ({
            type: ADD_HOLIDAY,
            payload: res.data
        })
    } catch (err) {
        console.log(err);
    }

    }
    const updateHoliday = async (holiday) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        try {
          const res = await axios.put(`http://localhost:5000/holidays/${holiday._id}`, holiday, config);
          console.log(res)
            dispatch ({
                type: UPDATE_HOLIDAY,
                payload: res.data
            })
        } catch (err) {
            console.log(err)
        }
    }

    const removeHoliday = (id) => {
        dispatch ({
            type: REMOVE_HOLIDAY,
            payload: id
        })
    }
    
    const toggleFilter = () => {
        dispatch({
            type: TOGGLE_FILTER,

        })
    }

    const searchUser = (user) => {
        dispatch ({
            type: SEARCH_USER,
            payload: user
        })
    }
   

    const clearSearch = () => {
        dispatch ({
        type: CLEAR_SEARCH,
        })

    }

    const approveHoliday = (holiday) => {
        holiday.status = true;
        dispatch ({
            type: APPROVE_HOLIDAY,
            payload: holiday
        })
    }

    
    return (
        <HolidayContext.Provider value={{
            holidays: state.holidays,
            filterGuest: state.filterGuest,
            users: state.users,
            search: state.search,
            getHolidays,
            toggleFilter,
            searchUser,
            clearSearch,
            addHoliday,
            removeHoliday,
            approveHoliday,
            updateHoliday
        }}>{props.children}</HolidayContext.Provider>

    )
}
export default HolidayState

