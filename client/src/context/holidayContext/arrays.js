const HolidayState = (props) => {
    const initialState = {
        filterGuest: false,
        search: null,
        holidays: [{
            id: "1",
            type:"Sick",
            title: "Corona",
            comment: "Hello, I have tested positive for Corona virus and therefore will not be in the workplace for the next 3 weeks at least",
            date:"12 May, 2021",
            status: true,
            status_date: "13 May, 2021"

        },
        {
            id: "2",
            type:"Holiday",
            title: "Vacation",
            comment: "Taking some days off",
            date:"12 March, 2021",
            status: true,
            status_date: "13 March, 2021"

        },
        {
            id: "3",
            type:"Holiday",
            title: "Corona",
            comment: "Going to Finland",
            date:"12 May, 2021",
            status: false,
            status_date: "13 May, 2021"

        },
        {
            id: "4",
            type:"Business Trip",
            title: "Conference with colleagues in Stockholm",
            comment: "Going to Stockholm to attend the yearly PI Planning conference for a week",
            date:"21 May, 2021",
            status: false,
            status_date: "13 May, 2021"

        }],
        users: [
            {
                name:  "Signe",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/women/65.jpg"

            },
            {
                name:  "Anne",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/women/34.jpg"
                
            },
            {
                name:  "Daniel",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/men/9.jpg"
                
            },
            {
                name:  "Soren",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/men/78.jpg"
                
            },
            {
                name:  "Simon",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/men/67.jpg"
                
            },
            {
                name:  "Signe",
                occupation: "Developer",
                country: "Sweden",
                mobile: "+45 283993424",
                laptop: "G3413485",
                usertype: "user",
                image : "https://randomuser.me/api/portraits/women/52.jpg"
                
            }
        ],
    }
}